import { MinLength, IsPositive } from 'class-validator';
export class CreateProductDto {
  @MinLength(8)
  name: string;

  @IsPositive()
  price: number;
}
